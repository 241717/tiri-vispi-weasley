import json
from .models import *


def cookieCart(request):
    try:
        carrello = json.loads(request.COOKIES['carrello'])
    except:
        carrello = {}

    print('carrello: ', carrello)
    items = []
    order = {'get_cart_total': 0, 'get_cart_items': 0, 'shipping': False}
    cartItems = order['get_cart_items']

    for i in carrello:
        try:
            cartItems += carrello[i]["quantity"]

            prodotto = Prodotto.objects.get(id=i)
            total = (prodotto.price * carrello[i]["quantity"])

            order['get_cart_total'] += total
            order['get_cart_items'] += carrello[i]["quantity"]

            item = {
                'prodotto': {
                    'id': prodotto.id,
                    'name': prodotto.name,
                    'price': prodotto.price,
                    'imageURL': prodotto.imageURL,
                },
                'quantity': carrello[i]["quantity"],
                'get_total': total
            }
            items.append(item)

            if prodotto.digital == False:
                order['shipping'] = True
        except:
            pass
    return {'cartItems': cartItems, 'order': order, 'items': items}


def cartData(request):
    if request.user.is_authenticated:
        user = request.user
        order, created = Order.objects.get_or_create(user=user, complete=False)
        items = order.orderitem_set.all()
        cartItems = order.get_cart_items
    else:
        cookieData = cookieCart(request)
        cartItems = cookieData['cartItems']
        order = cookieData['order']
        items = cookieData['items']
    return {'cartItems': cartItems, 'order': order, 'items': items}


def ordineGuest(request, data):
    print('utente non loggato...')
    print('Cookies: ', request.COOKIES)
    name = data['form']['name']
    email = data['form']['email']

    cookieData = cookieCart(request)
    items = cookieData['items']

    user, created = User.objects.get_or_create(
        email=email,
    )
    user.name = name
    user.save()

    order = Order.objects.create(
        user=user,
        complete=False,
    )

    for item in items:
        prodotto = Prodotto.objects.get(id=item['prodotto']['id'])

        orderItem = OrderItem.objects.create(
            prodotto=prodotto,
            order=order,
            quantita=item['quantity']
        )
    return user, order
