from django.contrib.auth.decorators import login_required
from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.store, name="store"),
    path('carrello/', views.carrello, name="carrello"),
    path('checkout/', views.checkout, name="checkout"),
    path('update_item/', views.updateItem, name="update_item"),
    path('process_order/', views.processOrder, name="process_order"),
    path('<int:id>/', views.product_detail, name='product_detail'),
    path('cerca/', views.cerca, name='cerca'),
    path('profile', views.MyorderConf, name='profile'),
    re_path(r'^password/$', views.PasswordChangeView, name='modica-password'),
    path('Myorder/', views.MyorderConf, name='Myorder'),
    path('add-item', views.ItemAdd.as_view(), name='item-add'),
    path('order-conf', views.orderConf, name='order-conf'),
    path('order-conf-ajax', views.orderConfajax, name='order-conf-ajax'),
]
