# Create your tests here.
from decimal import Decimal

from django.test import TestCase
from django.urls import reverse
# test view
from store.models import *


class TestViews(TestCase):

    def setUp(self):
        self.user_test = User.objects.create(email='test@test.it', first_name='test_user',
                                             last_name='test_last', password='test')
        self.items = Prodotto.objects.create(name='Titolo', price='0', digital='False',
                                             description='Descrizione', )

    def test_model(self):
        item_test = Prodotto()
        item_test.name = "test da prova"
        item_test.price = 0
        item_test.digital = False
        item_test.image = None
        item_test.description = "descrizione"
        item_test.save()
        record = Prodotto.objects.get(id=item_test.id)
        self.assertEqual(record, item_test)

    def test_order_item(self):
        order_item_test = OrderItem()
        order_item_test.user = self.user_test
        order_item_test.ordered = True
        order_item_test.item = self.items
        order_item_test.quantity = 4
        order_item_test.save()
        record = OrderItem.objects.get(pk=order_item_test.pk)
        self.assertEqual(record, order_item_test)

    def test_itemAdd(self):
        self.client.login(email='admin', password='admin')
        url = 'item-add'
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def test_login(self):
        self.client.login(email='admin', password='admin')
        response = self.client.get(reverse('account:logout'))
        self.assertEquals(response.status_code, 302)

    def test_profile_view(self):
        self.client.login(email='admin', password='admin')
        response = self.client.get(reverse('profile'))
        self.assertEquals(response.status_code, 302)
