import datetime
import json

from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView

from account.forms import ItemCrispyForm
from .models import *
from .utils import cartData, ordineGuest


def store(request):
    data = cartData(request)
    cartItems = data['cartItems']

    prodotto = Prodotto.objects.all

    context = {'prodotto': prodotto, 'cartItems': cartItems}
    return render(request, 'store/store.html', context)


def carrello(request):
    data = cartData(request)
    cartItems = data['cartItems']
    order = data['order']
    items = data['items']

    context = {'items': items, 'order': order, 'cartItems': cartItems}

    return render(request, 'store/carrello.html', context)


def checkout(request):
    data = cartData(request)

    cartItems = data['cartItems']
    order = data['order']
    items = data['items']

    context = {'items': items, 'order': order, 'cartItems': cartItems}
    return render(request, 'store/checkout.html', context)


def updateItem(request):
    data = json.loads(request.body)
    prodottoId = data['prodottoId']
    action = data['action']
    print('Action:', action)
    print('Product:', prodottoId)

    user = request.user
    prodotto = Prodotto.objects.get(id=prodottoId)
    order, created = Order.objects.get_or_create(user=user, complete=False)

    orderItem, created = OrderItem.objects.get_or_create(order=order, prodotto=prodotto)

    if action == 'add':
        orderItem.quantita = (orderItem.quantita + 1)
    elif action == 'remove':
        orderItem.quantita = (orderItem.quantita - 1)

    orderItem.save()

    if orderItem.quantita <= 0:
        orderItem.delete()

    return JsonResponse('Item was added', safe=False)


def processOrder(request):
    transaction_id = datetime.datetime.now().timestamp()
    data = json.loads(request.body)

    if request.user.is_authenticated:
        user = request.user
        order, created = Order.objects.get_or_create(user=user, complete=False)

        if order.shipping == True:
            ShippingAddress.objects.create(
                user=user,
                order=order,
                indirizzo=data['shipping']['indirizzo'],
                citta=data['shipping']['citta'],
                provincia=data['shipping']['provincia'],
                cap=data['shipping']['cap'],

            )

    else:
        user, order = ordineGuest(request, data)

    total = float(data['form']['total'])
    order.transaction_id = transaction_id

    # serve per evitare che un utente riesca a comprare il prodotto pagandolo alla cifra che vuole, andiamo a controllare se il totale che viene pagato
    # è uguale al totale del carrello e solo in tal caso l'ordine sarà poi processato

    if total == order.get_cart_total:
        order.complete = True
    order.save()

    if order.shipping == True:
        ShippingAddress.objects.create(
            user=user,
            order=order,
            indirizzo=data['shipping']['indirizzo'],
            citta=data['shipping']['citta'],
            provincia=data['shipping']['provincia'],
            cap=data['shipping']['cap'],
        )

    return JsonResponse('pagamento completo', safe=False)


def product_detail(request, id):
    prodotto = get_object_or_404(Prodotto,
                                 id=id,
                                 )

    return render(request,
                  'store/detail.html',
                  {'prodotto': prodotto
                   })


def cerca(request, ):
    """
    Barra di ricerca
    :return: ritorna la pagina che mostra i risultati della ricerca
    """
    if "q" in request.GET:

        querystring = request.GET.get("q")
        # print(querystring)
        if len(querystring) == 0:
            return redirect("/cerca/")
        prodotti = Prodotto.objects.filter(name__icontains=querystring)
        print(prodotti)
        context = {"prodotti": prodotti}
        return render(request, 'store/cerca.html', context)
    else:
        return render(request, 'store/cerca.html')


@login_required(login_url='/login')
def ProfileView(request):
    form = []
    user = request.user.id
    context1 = {"profile": Profile.objects.get(user=request.user)}
    return render(request, 'store/profile.html', context1)


class PasswordChange(object):
    pass


@login_required(login_url='/login')
def PasswordChangeView(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Password modificata con Successo!')
            return render(request, 'store/store.html')
        else:
            messages.error(request, 'Correggi gli errori e riprova.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'store/password_change.html', {
        'form': form
    })


def orderConfajax(request):
    # product = Product.objects.get(id=id)
    # print(request.POST.get('username'))
    data = {'is_taken': False}
    if request.method == 'POST' and request.user.is_authenticated:
        tt = Order.objects.get(id=request.POST.get('username'))
        tt.complete = True
        tt.save()

        data = {'is_taken': True}

        return JsonResponse(data)
    return JsonResponse(data)


@login_required(login_url='/login')
def orderConf(request):
    now = datetime.datetime.now()
    start_date = now.strftime("%m")
    start_date = str(int(start_date) - 1)

    print(start_date, "startdate")
    items = {}
    for x in Order.objects.filter(data_ordine__month=start_date):
        items[int(x.data_ordine.strftime("%d"))] = 0

    for x in Order.objects.filter(data_ordine__month=start_date):
        items[int(x.data_ordine.strftime("%d"))] = x.items.count() + items[int(x.data_ordine.strftime("%d"))]

    list = [(k, v) for k, v in items.items()]
    print(items)

    norder = Order.objects.filter(complete=True)

    norder2 = Order.objects.filter(complete=False)

    context = {'items': list, 'norder': norder, 'norder2': norder2}
    return render(request, 'store/totordini.html', context)


@login_required(login_url='/login')
def MyorderConf(request):
    norder = Order.objects.filter(user=request.user)
    print(norder)
    context = {'norder': norder}
    return render(request, 'store/profile.html', context)



class ItemAdd(CreateView):
    model = Prodotto
    template_name = 'store/add-item.html'
    form_class = ItemCrispyForm

    success_url = reverse_lazy('store')
