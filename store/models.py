from django.db import models
from django.contrib.auth.models import User
from store.models import *


class Prodotto(models.Model):
    name = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    digital = models.BooleanField(default=False, null=True, blank=False)
    image = models.ImageField(null=True, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

    @property
    def imageURL(self):
        try:
            url = self.image.url
            return url
        except:
            url = 'static/immagini/no_image.png'
        return url


class Item(models.Model):
    prodotto = models.ForeignKey(Prodotto, on_delete=models.SET_NULL, null=True)
    quantita = models.IntegerField(default=0, null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)

    def get_name(self):
        return self.prodotto.name

    @property
    def get_total(self):
        total = self.prodotto.price * self.quantita
        return total


class Order(models.Model):
    # facciamo la relazione tra il cliente e l'ordine, se l'utente è eliminato non viene eliminato l'ordine
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)  # un solo profilo per cliente
    data_ordine = models.DateTimeField(auto_now_add=True)
    complete = models.BooleanField(default=False)
    transaction_id = models.CharField(max_length=100, null=True)
    items = models.ManyToManyField(Item, blank=True)

    def __str__(self):
        return str(self.id)

    @property
    def shipping(self):
        shipping = False
        orderitems = self.orderitem_set.all()
        for i in orderitems:
            if i.prodotto.digital == False:
                shipping = True
        return shipping

    def get_total(self):
        total = 0
        for order_item in self.items.all():
            total += order_item.get_final_price()
        return total

    @property
    def get_cart_total(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.get_total for item in orderitems])
        return total

    @property
    def get_cart_items(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.quantita for item in orderitems])
        return total

    def getitems(self):
        total = "  "
        for order_item in self.orderitem_set.all():
            total += (order_item.get_name() + " pezzi: " + str(order_item.quantita)) + ", "
        return total


class OrderItem(models.Model):
    prodotto = models.ForeignKey(Prodotto, on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)  # un solo ordine può avere tanti item
    quantita = models.IntegerField(default=0, null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)

    @property
    def get_total(self):
        total = self.prodotto.price * self.quantita
        return total

    def get_name(self):
        return self.prodotto.name

    def get_qta(self):
        return self.quantita


class ShippingAddress(models.Model):
    # ci metto il cliente così se un ordine dovesse venir cancellato, mi resta cmq salvato l'indirizzo di spedizione del cliente
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)

    indirizzo = models.CharField(max_length=200, null=True)
    citta = models.CharField(max_length=200, null=True)
    provincia = models.CharField(max_length=200, null=True)
    cap = models.IntegerField(null=True)
    date_added = models.DateTimeField(auto_now_add=True)


def __str__(self):
    return self.id


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')

    def __str__(self):
        return self.user.first_name
