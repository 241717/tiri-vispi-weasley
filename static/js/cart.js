var updateBtns = document.getElementsByClassName('update-cart')

for (i=0; i < updateBtns.length; i++){
    updateBtns[i].addEventListener('click',function (){
        var prodottoId = this.dataset.prodotto
        var action = this.dataset.action
        console.log('prodottoId:', prodottoId, 'action:', action)

        console.log('utente: ', user)
        if (user == 'AnonymousUser'){
            addCookieItem(prodottoId, action)
        }else {
            updateUserOrder(prodottoId, action)
                }
    })
}

function addCookieItem(prodottoId, action){
	            console.log('utente non loggato...')
	if (action == 'add'){
		//prima controllo se il prodotto è già nel carrello, se non è allora lo aggiungo
		if (cart[prodottoId]== undefined){
			cart[prodottoId]= {'quantity':1}
		//se il prdotto era già presente allora aggiorno la quantità aggiungendone un altro
		}else{
			cart[prodottoId]['quantity'] += 1
		}
	}
	//se l'utente ha scelto di rimuovere il prodotto controlliamo se abbiamo solo un prodotto nel carrello, se abbiamo 1 solo prodotto allora lo andremmo a rimuovere del tutto
	if (action == 'remove'){
		cart[prodottoId]['quantity'] -= 1

		if (cart[prodottoId]['quantity']<= 0){
			console.log('prodotto non eliminabile')
			delete cart[prodottoId];
		}
	}
	console.log('carrello:', cart)
	document.cookie = 'carrello=' + JSON.stringify(cart) + ";domain=;path=/"
	location.reload()
}

function updateUserOrder(prodottoId, action){
	console.log('User is authenticated, sending data...')

		var url = '/update_item/'
		fetch(url, {
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				'X-CSRFToken':csrftoken,
			},
			body:JSON.stringify({"prodottoId":prodottoId, "action":action})
		})
		.then((response) => {
		   return response.json();
		})
		.then((data) => {
		    console.log('Data:', data)
			location.reload()
		});
}