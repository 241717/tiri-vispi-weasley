from django.forms import ModelForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from store.models import Prodotto


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password2']


class ItemCrispyForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'cheese-crispy-form'
    helper.form_method = 'POST'
    helper.add_input(Submit('submit', 'Salva'))

    class Meta:
        model = Prodotto
        fields = ('name', 'price', 'digital', 'image', 'description')
        labels = {'name': 'Titolo', 'price': 'Prezzo', 'description': 'Descrizione', 'image': 'Immagine',
                  'digital': 'è un prodotto digitale?'}
