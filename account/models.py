from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


class UserManager(BaseUserManager):
    def create_user(
            self, email, first_name, last_name, password=None,
            commit=True):
        """
        Creates and saves a User with the given email, first name, last name
        and password.
        """
        if not email:
            raise ValueError(_('Users must have an email address'))
        if not first_name:
            raise ValueError(_('Users must have a first name'))
        if not last_name:
            raise ValueError(_('Users must have a last name'))

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        if commit:
            user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password):
        """
        Creates and saves a superuser with the given email, first name,
        last name and password.
        """
        user = self.create_user(
            email,
            password=password,
            first_name=first_name,
            last_name=last_name,
            commit=False,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Hai inserito caratteri non consentiti (@,.,!,/)')
    username = models.CharField(_('username'), max_length=30, blank=True, validators=[alphanumeric])
    email = models.EmailField(
        verbose_name=_('email address'), max_length=255, unique=True
    )

    first_name = models.CharField(_('first name'), max_length=30, blank=True, validators=[alphanumeric])
    last_name = models.CharField(_('last name'), max_length=150, blank=True, validators=[alphanumeric])
    is_active = models.BooleanField(default=True)
   # staff = models.BooleanField(default=False)  # a admin utente; non super-utente
    admin = models.BooleanField(default=False)  # a superuser

    date_joined = models.DateTimeField(
        _('date joined'), default=timezone.now
    )

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def str(self):
        return '{} <{}>'.format(self.get_full_name(), self.email)

    @property
    def is_admin_p(self):
        "Is the utente a admin member?"
        return self.admin
