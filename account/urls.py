from django.urls import path
from django.contrib.auth import views as auth_views
import account.views
from account import views

app_name = 'account'

urlpatterns = [

    path('loginn', views.loginPage, name='login'),
    path('registrati', views.paginaRegistrazione, name='registrati'),
    path('logout/', views.logoutUser, name="logout"),
    ]
